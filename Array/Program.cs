﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hoofdmenu
            do
            {
                Console.WriteLine("****************************");
                Console.WriteLine("**  Dit is het hoofdmenu  **");
                Console.WriteLine("****************************");
                Console.WriteLine();
                Console.WriteLine("Gelieve een keuze te maken uit het volgende menu:");
                Console.WriteLine("1. U wenst een menu aan te maken.");
                Console.WriteLine("2. U wenst een bestaand menu aan te passen.");
                Console.WriteLine("3. U wenst het bestaande menu te verwijderen.");
                Console.WriteLine("4. U wenst deze applicatie te verlaten.");
                Console.WriteLine();
                Console.Write("Geef uw getal in: " );

                switch (Console.ReadLine())
                {
                    case "1":
                       MenuToevoegen();
                     break;  
                    case "2":
                        MenuAanpassen();     
                     break;  
                    case "3": //Menu verwijderen 
                        Console.Clear();
                        if (File.Exists("Lunch.txt"))
                        {
                            File.Delete("Lunch.txt");
                            Console.WriteLine("Het bestand is verwijderd.");
                        }
                        else 
                        {
                            Console.WriteLine("Er is geen bestand aanwezig met de naam 'Lunch.txt', gelieve eerst een menu aan te maken.");
                        }
                        Console.ReadLine();
                        Console.Clear();
                     break;  
                    case "4":// Menu verlaten                      
                        Console.WriteLine("Dank u voor het aanmaken van een menu. We wensen u nog een fijne dag.");
                        Console.ReadLine();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Gelieve een getal in te geven tussen 1 en 4 aub.");
                        break;
                }
            } while (true);

        }

        private static void MenuToevoegen()
        {
            Console.Clear();
            // Wat hebben we nodig.
            string[] arrDagen = new string[] { "maandag", "dinsdag", "woensdag", "donderdag","vrijdag" };
            string[] arrMenu = new string[5];

            // Menu ingeven
            int x = 0;
            foreach (string i  in arrDagen)
            {
                Console.WriteLine("Gelieve het menu voor " + i + " in te geven aub:");
                arrMenu[x] = Console.ReadLine();
                x++;
               
            }

            //Tonen van de ingegeven menu's
            Console.Clear();
            Console.WriteLine("Dit zijn de menu's voor de komende week dat u heeft ingegeven: ");
            for (int i = 0; i < arrDagen.Length; i++)
            {
                string Result = arrDagen[i] + ": " + arrMenu[i];
                Console.WriteLine(Result);
               
            }
            //Schrijf het menu weg naar een bestand
            MenuBewaren(arrMenu, true);
            Console.ReadLine();
            Console.Clear();

        }    // OK
        private static void MenuAanpassen()
        {
            if (File.Exists("Lunch.txt"))
            {
                Console.Clear();
                Console.WriteLine("Dit is het ingegeven menu voor de komende week: \n");
                StreamReader sr = new StreamReader("Lunch.txt");
                string inhoud = sr.ReadToEnd();
                sr.Close();
                Console.Write(inhoud);
                Console.ReadLine();
                Console.Clear();

                do
                {
                    Console.WriteLine("Gelieve de dagnummer in te vullen: \n 0 = maandag\n 1 = dinsdag\n 2 = woensdag\n 3 = donderdag\n 4 = vrijdag");
                    string dagnummer = Console.ReadLine();

                } while (true);
            }
            else
            {
                Console.WriteLine("Er is geen menu aanwezig. Gelieve een menu aan te maken.");
            }
            
        }
        private static void MenuBewaren(string[] arrMenu, bool addDays)
        {
     
            if (File.Exists("Lunch.txt"))
            {
                Console.WriteLine("File bestaat, wenst u deze te overschrijven? y/n ");
                string input = Console.ReadLine().ToLower();
                if (input == "y")
                {
                    Console.WriteLine("De file wordt overschreven.");
                    File.Delete("Lunch.txt");
                    int tellerke = 0;
                    using (StreamWriter writer = new StreamWriter("Lunch.txt"))
                    {
                        if (addDays)
                        {
                            foreach (string item in arrMenu)
                            {
                                switch (tellerke)
                                {
                                    case 0:
                                        writer.WriteLine("maandag: ", item);
                                        break;
                                    case 1:
                                        writer.WriteLine("dinsdag: " + item);
                                        break;
                                    case 2:
                                        writer.WriteLine("woensdag: " + item);
                                        break;
                                    case 3:
                                        writer.WriteLine("donderdag: " + item);
                                        break;
                                    case 4:
                                        writer.WriteLine("vrijdag: " + item);
                                        break;
                                    default:
                                        break;
                                }

                                tellerke++;
                            }
                        }
                        else
                        {
                            foreach (string item in arrMenu)
                            {
                                switch (tellerke)
                                {
                                    case 0:
                                        writer.WriteLine(item);
                                        break;
                                    case 1:
                                        writer.WriteLine(item);
                                        break;
                                    case 2:
                                        writer.WriteLine(item);
                                        break;
                                    case 3:
                                        writer.WriteLine(item);
                                        break;
                                    case 4:
                                        writer.WriteLine(item);
                                        break;
                                    default:
                                        break;
                                }
                                tellerke++;
                            }
                        }
                    }
                }
                else if (input == "n")
                {
                    Console.WriteLine("Het menu is niet bewaard.");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("De file werd niet gevonden. We maken het aan.\n");
                using (StreamWriter writer = new StreamWriter("Lunch.txt"))
                {
                    Console.WriteLine("Uw bestand is aangemaakt.\n");
                    int tellerke = 0;

                    foreach (string item in arrMenu)
                    {
                        tellerke++;
                    }
                    Console.WriteLine("Uw gegevens zijn wegschreven.");
                    writer.Close();
                }
            }
        } //OK
        
        
    }
}
